"""
Copyright 2018 Vitaliy Urusovskij

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


import cv2
import subprocess
import numpy as np

# Define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'XVID')


def set_video_writer(name, img_shape, FPS):
    return cv2.VideoWriter(name, fourcc, FPS, img_shape)


def set_FPS(video):
    # Find OpenCV version
    (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
    try:
        if int(major_ver) < 3:
            fps = video.get(cv2.cv.CV_CAP_PROP_FPS)
        else:
            fps = video.get(cv2.CAP_PROP_FPS)
    except ReferenceError:
        print("Can't set FPS to output video")
    if fps < 1:
        raise ValueError("FPS have wrong value")

    return fps


def set_img_shape(video):
    ret_code, frame = video.read()
    height, width, _ = frame.shape
    return (height, width)


def write_audio_from_input_video(video_path, audio_path):
    cmd = ['ffmpeg', '-i', video_path, '-vn', audio_path]
    pipe = subprocess.Popen(args=cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return pipe


def write_final_video(video_path, audio_path, output_path):
    cmd = ['ffmpeg', '-i', video_path, '-i', audio_path, output_path]
    pipe = subprocess.Popen(args=cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    ret_code = pipe.wait()
    if ret_code != 0:
        raise RuntimeError("Error while compiling buffered video and audio in output video")


def preprocess_image(image_path, img_size=None, scale=1, mean=0, flip=False):
    if flip:
        img = cv2.cvtColor(cv2.imread(image_path), cv2.COLOR_BGR2RGB)
    else:
        img = cv2.imread(image_path)
    if img_size is not None:
        img = cv2.resize(img, img_size)
    img = img.astype(np.float32)
    img = img - mean
    img = img / scale
    return img