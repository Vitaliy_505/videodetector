"""
Copyright 2018 Vitaliy Urusovskij

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


import cv2
import numpy as np


def cv2_load_model_from_Caffe(model, weights):
    return cv2.dnn.readNetFromCaffe(weights, model)


def cv2_load_model_from_TensorFlow(model, weights):
    return cv2.dnn.readNetFromTensorflow(model)


def cv2_detect_from_Caffe(frame, net, net_shape, classes, colors):
    blob = cv2.dnn.blobFromImage(cv2.resize(frame, net_shape), 0.007843, net_shape, 127.5)
    net.setInput(blob)
    detections = net.forward()
    (h, w) = frame.shape[:2]
    for i in np.arange(0, detections.shape[2]):
        confidence = detections[0, 0, i, 2]
        if confidence > 0.2:
            idx = int(detections[0, 0, i, 1])
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")

            # draw the prediction on the frame
            label = "{}: {:.2f}%".format(classes[idx],
                                         confidence * 100)
            cv2.rectangle(frame, (startX, startY), (endX, endY),
                          colors[idx], 2)
            y = startY - 15 if startY - 15 > 15 else startY + 15
            cv2.putText(frame, label, (startX, y),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, colors[idx], 2)