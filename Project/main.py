"""
Copyright 2018 Vitaliy Urusovskij

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


import argparse
import os


from utils import *
from cv2_detection import *


CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
 "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
 "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
 "sofa", "train", "tvmonitor", "bear", "elephant", "zebra",
 "giraffe", "truck"]
COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))


def set_example_values(args):
    print("[ INFO ] Run example")
    basedir = os.path.dirname(os.path.abspath(__file__))
    args.input_video = os.path.join(basedir, "example", "example.mp4")
    args.model = os.path.join(basedir, "example", "example.caffemodel")
    args.weights = os.path.join(basedir, "example", "example.prototxt")
    args.net_shape = (300, 300)


def main(input_video, model, weights, net_shape, load_model, detect_objects_on_frame, output_video='output.mp4'):

    buff_video = os.path.splitext(output_video)[0] + '_buff' + os.path.splitext(output_video)[1]
    buff_audio = os.path.splitext(output_video)[0] + '_buff' + '.mp3'

    audio_pipe = write_audio_from_input_video(input_video, buff_audio)

    video_capture = cv2.VideoCapture(input_video)
    FPS = set_FPS(video_capture)
    img_shape = set_img_shape(video_capture)

    print("[ INFO ] Loading model")
    net = load_model(model, weights)

    print("[ INFO ] Starting video processing")
    video_writer = set_video_writer(name=buff_video, FPS=FPS, img_shape=(img_shape[1], img_shape[0]))
    while video_capture.isOpened():
        ret_code, frame = video_capture.read()

        if ret_code:
            detect_objects_on_frame(frame=frame, net=net, net_shape=net_shape, classes=CLASSES, colors=COLORS)
            video_writer.write(frame)
        else:
            break
    print("[ INFO ] Detection is complete")
    video_capture.release()
    video_writer.release()

    ret_code = audio_pipe.wait()
    if ret_code != 0:
        raise RuntimeError("Error while compiling buffered video and audio in output video")

    print("[ INFO ] Deleting buffered files")
    print("[ INFO ] Building final video")
    write_final_video(buff_video, buff_audio, output_video)
    print("[ SUCCESS ]")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Diploma')
    parser.add_argument('--input-video', type=str, help='Path to video file')
    parser.add_argument('--model', type=str,
                        help='Path to model. For Caffe: .caffemodel; For TensorFlow: .pb')
    parser.add_argument('--weights', type=str,
                        help='Path to weights with weights name. For Caffe: .prototxt')
    parser.add_argument('--framework', type=str,
                        help='name of the framework. Supported "opencv"')

    parser.add_argument('--rgb-mean', type=str, default='0,0,0', help='Mean values, '
                                                                      'required 3 values separated by comma')
    parser.add_argument('--rgb-scale', type=str, default='1,1,1', help='Scale values, '
                                                                       'required 3 values separated by comma')

    parser.add_argument('--net-shape', type=list, help='Shape of the image, '
                                                       'required list with 2 values')
    parser.add_argument('--example', action='store_true',
                        help='run example')
    args = parser.parse_args()
    print(args)
    if args.example:
        set_example_values(args)
    if args.framework.lower() == 'opencv':
        load_model = cv2_load_model_from_Caffe
        detect_objects_on_frame = cv2_detect_from_Caffe
    elif args.framework.lower() == 'tensorflow':
        load_model = cv2_load_model_from_TensorFlow
        detect_objects_on_frame = cv2_detect_from_TensorFlow
    main(input_video=args.input_video, model=args.model, weights=args.weights, net_shape=args.net_shape,
         load_model=load_model, detect_objects_on_frame=detect_objects_on_frame)
